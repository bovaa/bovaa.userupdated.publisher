from src.infrastructure.QueueProcessor import Processor
from src.infrastructure.LambdaInvoker import Invoker
import os

def lambda_handler(event, context):
    try:
        print('getting list of new users')
        print(event)

        processor = Processor(os.environ['aws_region'], os.environ['aws_access_key_id'],
                      os.environ['aws_secret_access_key'])
        message_list = processor.get_messages()

        if len(message_list) > 0:
            invoker = Invoker(os.environ['aws_region'], os.environ['aws_access_key_id'],
                              os.environ['aws_secret_access_key'])

            for message in message_list:
                invoker.publish(message)
        else:
            print('no messages to process')

    except Exception as e:
        print(e)