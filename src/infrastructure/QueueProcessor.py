import boto.sqs
import os

class Processor:

    def __init__(self, _aws_region, _aws_access_key_id, _aws_secret_access_key):

        self.client = boto.sqs.connect_to_region(_aws_region,
                                                 aws_access_key_id=_aws_access_key_id,
                                                 aws_secret_access_key=_aws_secret_access_key)

    def get_messages(self):
        print('in get_messages')
        try:
            queue_name = os.environ['account_key_requested_topic']
            max_messages_to_retrieve = int(os.environ['max_sqs_messages_to_retrieve'])

            q = self.client.get_queue(queue_name)
            q_resultset = q.get_messages(max_messages_to_retrieve) if q != None else []
            print('messages retrieved')
            message_list = []
            for m in q_resultset:
                print('message loop')
                message_list.append(m.get_body())
                m.delete()

            print('retrieved {} messages'.format(message_list))
            return message_list
        except Exception as e:
            print('error while getting queue')