import boto.sns
import json
import os

class Invoker:
    def __init__(self, aws_region, aws_access_key_id, aws_secret_access_key):

        self.client = boto.sns.connect_to_region(aws_region,
                                                 aws_access_key_id=aws_access_key_id,
                                                 aws_secret_access_key=aws_secret_access_key)

    def publish(self, data):
        try:
            print('in publish')
            payload = json.dumps(json.loads(data))

            topic = self.client.create_topic(os.environ['account_key_requested_sns'])
            self.client.publish(topic=topic["CreateTopicResponse"]["CreateTopicResult"]["TopicArn"],
                           message=payload)
        except Exception as e:
            print('Error in publishing sns message')
            print(e)