# bovaa.userupdated.publisher

This is a lambda function that runs on a scheduler.

# Environment Variables
````
account_key_requested_topic
account_key_requested_sns
max_sqs_messages_to_retrieve
aws_region
aws_access_key_id
aws_secret_access_key
````

# How the lambda works
This lambda polls the SNS topic specified in the environment variable account_key_requested_topic.
Sample body of the queue:
```
'{ "email": "b31@testmail.com",  "key": "v"}
```
It then publishes an SNS message to account_key_requested_sns.

